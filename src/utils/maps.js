const rules = {
    basic: val => !!val || 'Required',
    email: v => {
      if(v === '') {
        return 'Email field is required'
      } 
      return /.+@+/.test(v) || 'Please enter a valid Email address'},
}
  
  export const formMap = [
    {id: 1, name: 'name', label: 'First Name', type: 'text', rules: rules.basic},
    {id: 2, name: 'last_name', label: 'Last Name', type: 'text', rules: rules.basic},
    {id: 3, name: 'phone_number', label: 'Phone Number', type: 'text', rules: rules.basic},
    {id: 4, name: 'email', label: 'Email', type: 'email', rules: rules.email},
    {id: 5, name: 'country', label: 'Country', type: 'text', rules: rules.basic},
    {id: 6, name: 'city', label: 'City', type: 'text', rules: rules.basic},
    {id: 7, name: 'address', label: 'Address', type: 'text', rules: rules.basic}
  ]
  
  export const tableHeaderMap = [
      { text: 'First Name', value: 'name' },
      { text: 'Last Name', value: 'last_name' },
      { text: 'Phone Number', value: 'phone_number' },
      { text: 'Email', value: 'email' },
      { text: 'Country', value: 'country' },
      { text: 'City', value: 'city' },
      { text: 'Address', value: 'address' },
      { text: 'Actions', value: 'actions' }
  ]