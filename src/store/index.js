import Vue from 'vue'
import Vuex from 'vuex'

import ContactsService from '@/services/ContactsService.js'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    contacts: [],
    itemToEdit: null
  },
  mutations: {
    setContacts(state, contacts) {
      state.contacts = contacts
    },
    setItem(state, item) {
      state.itemToEdit = item
    },
    increaseTotal(state) {
      state.contacts.total++
    }
  },
  getters: {
    contacts:(state) => state.contacts,
    itemToEdit: state => state.itemToEdit
  },
  actions: {
    async loadAllContacts({commit}, payload) {
      try {
        const {data} = await ContactsService.getContacts(payload)
        commit('setContacts', data)
      } catch (e) {
        console.log(e);
      }
    },
    async addContact({commit}, contact ) {
      try {
        await ContactsService.addContact(contact)
        commit('increaseTotal')
      } catch (e) {
        console.log(e.message);
      }
    },
    async deleteContact(_, id) {
      try {
        await ContactsService.deleteContact(id)
      } catch (e) {
        console.log(e.message);
      }
    },
    async updateContact({ commit }, id) {
      try {
        await ContactsService.updateContact(id)
        commit('setItem', null)
        commit('increaseTotal')
      } catch(e) {
        console.log(e.message);
      }
    }
  },
})
