import axios from 'axios'

const apiClient = axios.create({
  baseURL: `http://test01.varid.pl:4080/api/`
})

export default {
  getContacts(payload) {
    return apiClient.get(`contacts?per_page=${payload.per_page}&page=${payload.page}`)
  },
  addContact(payload) {
    return apiClient.post('contact', payload) 
  },
  deleteContact(id) {
    return apiClient.delete(`contact/delete/${id}`)  
  },
  updateContact(payload) {
    return apiClient.put(`contact/${payload.id}`, payload)  
  }
}